exports.config = {
  files: {
    javascripts: {
      joinTo: {
        'js/app.js': /^src\/client/,
        'js/vendor.js': /^node_modules/
      }
    },
    stylesheets: {
      joinTo: 'css/app.css'
    }
  },

  paths: {
    public: './public',
    watched: ['src/client']
  },

  plugins: {
    babel: {
      presets: ['env', 'react']
    },
  },

  modules: {
    autoRequire: {
      'js/app.js': ['src/client/index']
    }
  },


};
