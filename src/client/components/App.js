import React from 'react';
import Inventory from './inventory/Inventory';
import BrewStand from './brew-stand/BrewStand';
import getInventory from '../data/get-inventory';
import postBrew from '../data/post-brew';
/**
 * Top level component.
 */
class App extends React.Component {
  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------
  constructor(props) {
    super(props);
    this.state = {
      inventory: [],
      brewStand: [],
      isBrewing: false,
      formula: '',
    };

    this.addToBrewStand = this.addToBrewStand.bind(this);
    this.removeFromBrewStand = this.removeFromBrewStand.bind(this);
    this.mix = this.mix.bind(this);
  }

  // --------------------------------------------------------------------------
  // Methods
  // --------------------------------------------------------------------------
  // Add an ingredient to brew stand
  addToBrewStand(id) {
    return () => {
      if (this.state.brewStand.length >= 3) {
        return;
      }

      if (this.state.brewStand.find((elem) => elem.id === id)) {
        return;
      }

      const toAdd = this.state.inventory.find((elem) => elem.id === id);
      if (toAdd && toAdd.quantity > 0) {
        this.setState((prevState) => ({
          brewStand: prevState.brewStand.concat([toAdd]),
          formula: ''
        }));
      }
    };
  }

  // Remove an ingredient from brew stand
  removeFromBrewStand(id) {
    return () => {
      this.setState((prevState) => ({
        brewStand: prevState.brewStand.filter((item) => item.id !== id),
      }));
    };
  }

  // Send a request to mix the ingredients
  mix() {
    const idBrewStand = this.state.brewStand.map((item) => item.id);
    postBrew(idBrewStand)
      .then((res) => {
        this.setState((prevState) => ({
          inventory: prevState.inventory.map((item) => idBrewStand.includes(item.id) ? {id: item.id, name: item.name, quantity: item.quantity - 1} : item),
          brewStand: [],
          formula: res.body.message
        }));
      })
      .catch((err) => {
        this.setState(() => ({
          formula: err.response
        }));
      });
  }

  // --------------------------------------------------------------------------
  // Lifecycle
  // --------------------------------------------------------------------------
  componentDidMount() {
    getInventory()
      .then((res) => {
        this.setState((prevState) => ({
          inventory: res.body.data,
        }));
      });
  }

  // --------------------------------------------------------------------------
  // Rendering
  // --------------------------------------------------------------------------
  render() {
    return (
      <div className="App">
        <div className="App-Column">
          <Inventory inventory={this.state.inventory} addHandler={this.addToBrewStand} />
        </div>
        <div className="App-Column">
          <BrewStand items={this.state.brewStand} isBrewing={this.state.isBrewing} removeHandler={this.removeFromBrewStand} mixHandler={this.mix} formula={this.state.formula}/>
        </div>
      </div>
    );
  }
}

export default App;
