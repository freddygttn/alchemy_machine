import React from 'react';
import PropTypes from 'prop-types';

/**
 * Displays the brew stand.
 */
const BrewStand = ({ items, isBrewing, formula, removeHandler, mixHandler }) => {
  return (
    <div className="BrewStand">
      <div>Ingrédient séléctionnés :</div>
      <ul>
        {
          items.map((item, index) => {
            return <li className="BrewStand-Item" key={index} onClick={removeHandler(item.id)}>{item.name}</li>;
          })
        }
      </ul>
      <button className="BrewStand-Button" disabled={items.length !== 3 || isBrewing} onClick={mixHandler}>Mixer !</button>
      <div className="BrewStand-Formula">{formula}</div>
    </div>
  );
};

BrewStand.propTypes = {
  items: PropTypes.array.isRequired,
  isBrewing: PropTypes.bool.isRequired,
  formula: PropTypes.string.isRequired,
  removeHandler: PropTypes.func.isRequired,
  mixHandler: PropTypes.func.isRequired
};

export default BrewStand;
