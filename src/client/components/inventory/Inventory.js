import React from 'react';
import PropTypes from 'prop-types';

import InventoryItem from './InventoryItem';

/**
 * An inventory item.
 */
const Inventory = ({ inventory, addHandler }) => {
  return (
    <div className="Inventory">
      <div className="Inventory-Title">Inventaire</div>
      <ul>
        {
          inventory.map((item, index) => {
            return <InventoryItem
              key={index}
              name={item.name}
              quantity={item.quantity}
              onClick={addHandler(item.id)} />;
          })
        }
      </ul>
    </div>
  );
};

Inventory.propTypes = {
  inventory: PropTypes.array.isRequired,
  addHandler: PropTypes.func.isRequired
};

export default Inventory;
