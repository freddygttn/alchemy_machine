import React from 'react';
import PropTypes from 'prop-types';

/**
 * Displays inventory items.
 */
const InventoryItem = ({ name, quantity, onClick }) => {
  return (
    <li className="InventoryItem" onClick={onClick}>{name} ({quantity})</li>
  );
};

InventoryItem.propTypes = {
  name: PropTypes.string.isRequired,
  quantity: PropTypes.number.isRequired,
  onClick: PropTypes.func
};

export default InventoryItem;
