import request from 'superagent';
import ApiConfig from './api-config';

// Retrieve the inventory.
export default () => {
  return request
    .get(`${ApiConfig.Enpoint}/inventory`)
    .set('Accept', 'application/json');
};
