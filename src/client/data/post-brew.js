import request from 'superagent';
import ApiConfig from './api-config';

// Retrieve the inventory.
export default (ingredients) => {
  return request
    .post(`${ApiConfig.Enpoint}/brew`)
    .set('Accept', 'application/json')
    .send({ingredients});
};
