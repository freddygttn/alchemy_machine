module.exports = [
  { ingredients: [38, 44, 45], result: 'Invisibilité'},
  { ingredients: [1, 2, 3], result: 'Force'},
  { ingredients: [1, 16, 30], result: 'Vitesse'},
  { ingredients: [1, 17, 31], result: 'Vol'},
  { ingredients: [2, 18, 32], result: 'Explosion'},
  { ingredients: [3, 18, 33], result: 'Dégivrant'},
  { ingredients: [4, 19, 34], result: 'Philtre d\'amour'},
  { ingredients: [5, 20, 35], result: 'Coca-cola'},
  { ingredients: [5, 21, 36], result: 'Un truc bizarre'},
  { ingredients: [6, 22, 37], result: 'Poil à gratter'},
  { ingredients: [7, 22, 38], result: 'Poison incapacitant'},
  { ingredients: [8, 23, 39], result: 'Simple précipité vert'},
  { ingredients: [9, 24, 39], result: 'Détartrant'},
  { ingredients: [10, 25, 40], result: 'Réspiration aquatique'},
  { ingredients: [11, 26, 40], result: 'Voix aigüe'},
  { ingredients: [12, 26, 41], result: 'Hilarité'},
  { ingredients: [13, 27, 41], result: 'Métamorphose en poisson'},
  { ingredients: [14, 27, 42], result: 'Télékinésie'},
  { ingredients: [15, 27, 43], result: 'Colle'},
  { ingredients: [15, 28, 44], result: 'Eau citronnée'},
  { ingredients: [15, 29, 45], result: 'Liquide vaiselle'}
];
