/**
 * Statefull module that manages the app state.
 */
const recipes = require('../data/recipes');
const ingredients = require('../data/ingredients');
const brewery = require('../domain/brewery');

const NoRecipeValue = 'Pas de formule';
let inventory = [
  10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
  10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
  10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
  10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
  10, 10, 10, 10, 10
];

const brew = (ingredientsToUse) => {
  // Return if cannot brew
  if (!brewery.canBrew(inventory, ingredientsToUse)) {
    return {status: 400, message: 'Ingredients non disponible'};
  }

  // Update the inventory
  inventory = brewery.updateInventory(inventory, ingredientsToUse);

  // Get the result
  return {status: 200, message: brewery.findBrewResult(recipes, ingredientsToUse, NoRecipeValue)};
};

const formatedInventory = () => {
  return ingredients.map((item, index) => {
    return {
      id: item.id,
      name: item.name,
      quantity: inventory[index]
    };
  });
};

module.exports = {
  brew,
  formatedInventory
};
