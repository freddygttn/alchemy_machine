/**
 * Check if a brew is possible given the inventory and ingredients.
 * @param {*} inventory The inventory to use
 * @param {*} ingredients The wanted ingredients to use
 */
const canBrew = (inventory, ingredients) => {
  if (ingredients.length === 0) {
    return false;
  }

  return ingredients
    .map((ingr) => inventory.length >= ingr ? inventory[ingr - 1] > 0 : false)
    .reduce((prev, curr) => prev && curr, true);
};

/**
 * Find the recipe given the ingredients.
 * Returns the noResultValue if none are found.
 * @param {*} recipes The recipes to check
 * @param {*} ingredients The ingredients used in the formula
 * @param {*} noResultValue The value to use if there is no formula
 */
const findBrewResult = (recipes, ingredients, noResultValue) => {
  const filteredRecipes = recipes.filter((recipe) =>
    recipe.ingredients.every((ingredient) => ingredients.includes(ingredient))
  );
  if (filteredRecipes.length === 0) {
    return noResultValue;
  }
  else {
    return filteredRecipes[0].result;
  }
};

/**
 * Updates the inventory for the given ingredients.
 * @param {*} inventory The inventory to update
 * @param {*} ingredients The ingredients to remove
 */
const updateInventory = (inventory, ingredients) => {
  if (ingredients.length === 0) {
    return inventory;
  }

  return inventory.map((elem, index) => {
    return ingredients.includes(index + 1) ? elem - 1 : elem;
  });
};

module.exports = {
  canBrew,
  findBrewResult,
  updateInventory
};
