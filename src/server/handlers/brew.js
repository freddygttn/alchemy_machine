const store = require('../domain/store');

/**
 * Brew handler.
 * @param {*} req The given request
 * @param {*} res The result to send
 */
module.exports = (req, res) => {
  let status = 400;
  let message = 'Données incorrectes';
  let data = req.body;
  // This logic should be extracted
  if (req.body &&
    req.body.ingredients &&
    Array.isArray(req.body.ingredients) &&
    req.body.ingredients.length === 3) {

    const brewData = store.brew(req.body.ingredients);
    status = brewData.status;
    message = brewData.message;

  }

  res.setHeader('Content-Type', 'application/json');
  res.status(status)
    .send(JSON.stringify({
      status,
      message,
      data
    }));
};
