const store = require('../domain/store');

/**
 * Brew handler.
 * @param {*} req The given request
 * @param {*} res The result to send
 */
module.exports = (req, res) => {
  const data = store.formatedInventory();
  res.setHeader('Content-Type', 'application/json');
  res.status(200)
    .send(JSON.stringify({
      status: 200,
      data
    }));
};
