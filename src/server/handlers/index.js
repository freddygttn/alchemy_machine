/**
 * Entrypoints for API.
*/
const express = require('express');
const router = express.Router();
const brewHandler = require('./brew');
const inventoryHandler = require('./inventory');

router.post('/brew', brewHandler);
router.get('/inventory', inventoryHandler);

module.exports = router;
