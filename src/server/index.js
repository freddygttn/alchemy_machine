const express = require('express');
const helmet = require('helmet');
const app = express();

const apiRouter = require('./handlers');

// Middlewares
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routes
app.use('/api', apiRouter);
app.use(express.static('public'));

// Listen !
app.listen(3000, () => /* console.log('http://localhost:3000') */ {} );
