const brewery = require('../src/server/domain/brewery');

describe('brewery', () => {
  describe('canBrew', () => {
    it('should say true if inventory is available', () => {
      const inventory = [ 10, 10, 10, 10, 10 ];
      expect(brewery.canBrew(inventory, [1, 2, 3])).toEqual(true);
    });

    it('should say false if inventory is not available', () => {
      const inventory = [ 0, 0, 0, 0, 0 ];
      expect(brewery.canBrew(inventory, [1, 2, 3])).toEqual(false);
    });

    it('should say false if inventory is not valid', () => {
      const inventory = [];
      expect(brewery.canBrew(inventory, [1, 2, 3])).toEqual(false);
    });

    it('should say false if ingredients are not valid', () => {
      const inventory = [ 0, 0, 0, 0, 0 ];
      expect(brewery.canBrew(inventory, [7, 8, 9])).toEqual(false);
    });

    it('should say false if ingredients are empty', () => {
      const inventory = [ 0, 0, 0, 0, 0 ];
      expect(brewery.canBrew(inventory, [])).toEqual(false);
    });
  });

  describe('findBrewResult', () => {
    it('should find the result if it exists', () => {
      const recipes = [ {ingredients: [1, 2, 3], result: 'A result'} ];
      expect(brewery.findBrewResult(recipes, [1, 2, 3], 'Nope')).toEqual('A result');
    });

    it('shouldn\'t care about ingredients order', () => {
      const recipes = [ {ingredients: [1, 2, 3], result: 'A result'} ];
      expect(brewery.findBrewResult(recipes, [3, 1, 2], 'Nope')).toEqual('A result');
    });

    it('should return the default value if no recipes are found', () => {
      const recipes = [ {ingredients: [1, 2, 3], result: 'A result'} ];
      expect(brewery.findBrewResult(recipes, [2, 3, 4], 'Nope')).toEqual('Nope');
    });

    it('should return the default value if no ingredients are given', () => {
      const recipes = [ {ingredients: [1, 2, 3], result: 'A result'} ];
      expect(brewery.findBrewResult(recipes, [], 'Nope')).toEqual('Nope');
    });
  });

  describe('updateInventory', () => {
    it('should return an updated inventory', () => {
      const inventory = [ 10, 10, 10, 10, 10 ];
      const expected = [ 9, 9, 9, 10, 10 ];
      expect(brewery.updateInventory(inventory, [1, 2, 3])).toEqual(expected);
    });

    it('should return given inventory if ingredients are not valid', () => {
      const inventory = [ 10, 10, 10, 10, 10 ];
      expect(brewery.updateInventory(inventory, [])).toEqual(inventory);
    });
  });
});
